<?php

use Illuminate\Database\Seeder;
use App\ParentOrganisation;

class ParentOrganisationsTableSeeder extends Seeder
{
    public function run()
    {
        $json = File::get("database/data/parent-organisations.json");
        $data = json_decode($json);

        foreach ($data as $parent_organisation) {
          ParentOrganisation::create(array(
            'id' => $parent_organisation->id,
            'name' => $parent_organisation->name,
            'organisations' => $parent_organisation->organisations
          ));
        }
    }
}
