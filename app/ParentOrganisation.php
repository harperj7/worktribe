<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentOrganisation extends Model {

	protected $table = 'parent_organisations';

    //To be used if new organisations are to be added/removed/updated etc.
    protected $fillable = ['name', 'organisations'];

    /**
     * Get the number of successful projects along belonging to each parent organisation
     * Also get the total value of all of the succesful projects for each parent organisation
     *
     * @return Array
     */
    public static function getNumberSuccessfulProjects() 
    {
        //Get all of the parent organisations
        $parents = ParentOrganisation::all();
        $count = 1;

        foreach ($parents as $parent) 
        {
            //The ids of the organisations belonging to the parent
            $org_ids = str_replace(['[', ']'], '', $parent->organisations);
            $org_ids = explode(',', $org_ids);

            //Dynamic variables used to store the total number of successful projects and total value
            ${"success_total_" . $count} = 0;
            ${"value_total_" . $count} = 0;

            foreach ($org_ids as $id) 
            {

                //Get the organisation information corresponding to the id
                $organisations = Organisation::where('id', '=', $id)->get();

                foreach ($organisations as $organisation) 
                {
                    //Get all of the projects that belong to this organisation
                    $projects = $organisation->projects;

                    //Parse the ids of the projects
                    $project_ids = str_replace(['[', ']'], '', $organisation->projects);
                    $project_ids = explode(',', $project_ids);

                    foreach ($project_ids as $project_id) 
                    {
                        
                        //Check to see if the project was successfully approved
                        $success = Project::where('id', '=', $project_id)->pluck('success');
                        
                        //Also get the value of this project
                        $value = Project::where('id', '=', $project_id)->pluck('value');
                        
                        //If it was successful increment the success number by 1 and add the value to the value total
                        if ($success == 1) 
                        {
                            ${"success_total_" . $count} += 1;
                            ${"value_total_" . $count} += $value;
                        }
                    }
                }
            }
            $count++; //Increment the counter by 1
        }

        $parent_organisations = [];

        for ($n = 1; $n < $count; $n++) //Add the retrieved information to an array
        {
            $parent_organisations[$n] = ${"success_total_" . $n} . ':' . ${"value_total_" . $n};
        }

        return $parent_organisations;
    }

}
