<?php

Route::get('/', 'PagesController@showHomePage');

Route::get('reports', ['as' => 'reports', 'uses' => 'ReportsController@index']);
Route::get('projects', ['as' => 'projects', 'uses' => 'ProjectsController@index']);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);