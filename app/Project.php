<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	protected $table = 'projects';

    //To be used if functionality was to be implemented allowing the adding of new projects etc.
    protected $fillable = ['title', 'description', 'img', 'value', 'project_lead_id', 'organisations'];

    /**
     * Return all of the projects that have been approved
     *
     * @return Object
     */
    public static function getSuccessfulProjects() 
    {
        return Project::where('success', '=', '1')->get();
    }
}