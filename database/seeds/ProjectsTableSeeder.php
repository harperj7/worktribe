<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    public function run()
    {
        $json = File::get("database/data/projects.json");
        $data = json_decode($json);

        foreach ($data as $project) {
          Project::create(array(
            'id' => $project->id,
            'title' => $project->title,
            'description' => $project->description,
            'success' => $project->success,
            'img' => $project->img,
            'value' => $project->value,
            'project_lead_id' => $project->project_lead_id,
            'organisations' => $project->organisations
          ));
        }
    }
}
