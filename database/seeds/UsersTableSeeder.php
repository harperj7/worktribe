<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $json = File::get("database/data/users.json");
        $data = json_decode($json);

        foreach ($data as $user) {
          User::create(array(
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'password' => Hash::make($user->password),
            'img' => $user->img,
            'organisation_id' => $user->organisation_id,
            'project_leads' => $user->project_leads
          ));
        }
    }
}
