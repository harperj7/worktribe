<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ParentOrganisation;
use App\Project;
use App\User;

class ReportsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the reports dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$orgs = ParentOrganisation::getNumberSuccessfulProjects();
		$org_names = [];
		$org_successes = [];
		$org_success_values = [];

		//Add retrieved information to corresponding arrays
		foreach ($orgs as $key => $value) 
		{
			$result = explode(':', $value);
			$org_successes[] = (int)$result[0];
			$org_success_values[] = (int)$result[1];
			$org_names[$key] = ParentOrganisation::where('id', '=', $key)->pluck('name');
		}

		$successful_projects = Project::getSuccessfulProjects();
		$leads = [];

		foreach ($successful_projects as $successful_project) 
		{
			//Check to see if the array index already exists and contains a value
			if (empty($leads[$successful_project->project_lead_id])) {
				$leads[$successful_project->project_lead_id] = $successful_project->value;
			} else {
				$leads[$successful_project->project_lead_id] += $successful_project->value;
			}
		}

		arsort($leads); //Sort the array in descending order
		$top_ten_leads = array_slice($leads, 0, 10, true); //Return only the first ten items maintaining the index

		foreach ($top_ten_leads as $k => $v) //Get the names corresponding to the project lead ids
		{
			$top_ten_names[] = User::where('id', '=', $k)->pluck('name');
		}
		
		//Return the reports index template - passing the processed data to the view
		return view('reports/index', compact('org_successes', 'org_names', 'org_success_values', 'top_ten_leads', 'top_ten_names'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
