<?php namespace App\Http\Controllers;

class PagesController extends Controller {

	/**
	 * Display the home page
	 *
	 * @return Response
	 */
	public function showHomePage()
	{
		return view('home');
	}

}
