@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <h1>Reports</h1>
            <div class="panel panel-default">
                <div class="panel-heading">Success rate for each organisation</div>

                <div class="panel-body">
                    <table id="success-rate"> 
                        <caption>Success rate for each organisation</caption> 
                        <caption id="legend">Organisations</caption> 
                        <thead><td></td><th>Number of successful projects</th></thead> 
                        <tbody> 
                            <?php $n = 1; ?>
                            @foreach ($org_successes as $org_success)
                                <tr><th>{{ $org_names[$n] }}</th><td>{{ $org_success }}</td></tr>
                                <?php $n++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Money received for each organisation</div>

                <div class="panel-body">
                    <table id="money-received"> 
                        <caption>Money received for each organisation</caption> 
                        <caption id="legend">Amount</caption> 
                        <thead><td></td><th>Organisation</th></thead> 
                        <tbody> 
                            <?php $x = 1; ?>
                            @foreach ($org_success_values as $org_success_value)
                                <tr><th>{{ $org_names[$x] }}</th><td>{{ $org_success_value }}</td></tr>
                                <?php $x++; ?>
                            @endforeach
                        </tbody> 
                        </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Top ten project leads (by amount of funding brought in)</div>

                <div class="panel-body">
                    <table id="project-leads"> 
                        <caption>Top ten project leads</caption> 
                        <caption id="legend">Amount</caption> 
                        <thead>
                            <?php $y = 0; ?>
                            @foreach ($top_ten_leads as $top_ten_lead)
                                <th>{{ $top_ten_names[$y] }}</th>
                                <?php $y++; ?>
                            @endforeach
                        </thead> 
                        <tbody> 
                            <tr>
                            <th>Project leads</th>
                            @foreach ($top_ten_leads as $top_ten_lead)
                                <td>{{ $top_ten_lead }}</td>
                            @endforeach
                            </tr>
                        </tbody> 
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('charts')
    <script src="js/graphix.0.9.min.js"></script>
    <script src="js/raphael-min.js"></script>
    <script type="text/javascript"> 
        $(function(){ 
            $("#success-rate").graphix({ type:'pie'});
            $("#money-received").graphix({ type:'line'});
            $("#project-leads").graphix({ type:'bar'});
            $("svg").parent().css( "margin", "0 auto" );
        }) 
    </script> 
@endsection