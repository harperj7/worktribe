<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;

use Request;
use Auth;
use DB;

class ProjectsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the projects dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		if (Request::input('filter')) {

			//Retrieve all of the current projects for the currently logged in user
			$projects = DB::table('projects')
							->join('users', 'projects.project_lead_id', '=', 'users.id')
							->select('projects.*', 'users.name')
							->where('project_lead_id', '=', Auth::id())
							->paginate(10);

		} else {
			//Retrieve all of the current projects
			$projects = DB::table('projects')
							->join('users', 'projects.project_lead_id', '=', 'users.id')
							->select('projects.*', 'users.name')
							->paginate(10);
		}

		//Display the projects index template - passing the retrieved projects to the view
		return view('projects/index', compact('projects'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
