@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">

				<div class="panel-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ut pharetra ex. Phasellus eu vestibulum nulla. In tempor nisi a porttitor venenatis. Curabitur volutpat gravida ex, in congue dolor condimentum eu. Fusce mollis aliquet nibh. Etiam malesuada sapien vel blandit luctus. Ut ut bibendum magna, ut blandit tortor. Curabitur sit amet elit quam. Quisque in nisl elementum, varius orci id, suscipit enim. Integer sit amet pharetra felis. Duis at est mollis, aliquet nunc ac, fermentum nunc. Curabitur orci libero, tristique eu eros id, efficitur maximus tortor. Ut posuere mauris non nulla convallis mollis.</p>

					<p>Nam eget risus commodo, feugiat ex vitae, pharetra erat. Aenean vitae volutpat sapien. Cras posuere sagittis justo in aliquam. Sed eget egestas ipsum. Morbi dignissim dolor sit amet pharetra maximus. Morbi pretium convallis nisl nec semper. Aliquam id augue blandit, laoreet ante quis, auctor arcu. Quisque vulputate tellus ac nibh dictum porta. Donec lacinia ipsum magna, faucibus pretium massa interdum id.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
