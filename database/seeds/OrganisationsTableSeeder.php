<?php

use Illuminate\Database\Seeder;
use App\Organisation;

class OrganisationsTableSeeder extends Seeder
{
    public function run()
    {
        $json = File::get("database/data/organisations.json");
        $data = json_decode($json);

        foreach ($data as $organisation) {
          Organisation::create(array(
            'id' => $organisation->id,
            'name' => $organisation->name,
            'users' => $organisation->users,
            'projects' => $organisation->projects,
            'parent_id' => $organisation->parent_id
          ));
        }
    }
}
