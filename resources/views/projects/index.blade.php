@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Projects 
                @if (!Request::input('filter'))
                <span class="filter"><a href="{!! route('projects', 'filter=true') !!}" title="Display only my projects">Display only my projects</a></span>
                @else
                    <span class="filter"><a href="{!! route('projects') !!}" title="Display all projects">Display all projects</a></span>
                @endif
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Value</th>
                                <th>Project lead</th>
                                <th>Successful?</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($projects as $project)
                                <tr>
                                    <td><img width="50" src="{!! $project->img !!}" /></td>
                                    <td>{{ $project->title }}</td>
                                    <td>{{ $project->description }}</td>
                                    <td>&pound;{{ number_format($project->value, 0) }}</td>
                                    <td>{{ $project->name }}</td>
                                    <td style="text-align:center">
                                        @if (!empty($project->success))
                                            <span title="Approved" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
                                        @else
                                            <span title="Unapproved" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div style="width:500px;margin:0 auto;">
                    {!! $projects->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection